import os

try:
    import setuptools
    import wheel
except ImportError:
    raise ImportError("You need 'setuptools' and 'wheel'. Install these libraries to continue.")


package_name = 'package_name'
os.mkdir(package_name)

setup_text = """import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="example-pkg-your-username",
    version="0.0.1",
    author="Example Author",
    author_email="author@example.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/pypa/sampleproject",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)"""

license_text = """Copyright (c) blah-blah-blah
"""

readme_text = """# blah_blah
"""

init_text = """name = '{}'""".format(package_name)

if len(os.listdir(path='./'.format(package_name))) == 0:
    raise FileNotFoundError('Folder {} is empty. Put your package here'.format(package_name))

with open('LICENSE', 'w+') as license:
    license.write(license_text)

with open('ReadMe.md', 'w+') as readme:
    readme.write(readme_text)

with open('setup.py', 'w+') as setup:
    setup.write(setup_text)

with open('{}/__init__.py'.format(package_name), 'w+') as init_file:
    init_file.write(init_text)

os.system('python3 setup.py sdist bdist_wheel')

print('Package {} was created successfully. Check "dist" folder to see resulting package.')

0
